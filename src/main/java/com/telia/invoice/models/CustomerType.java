package com.telia.invoice.models;

import javax.persistence.*;
import java.util.List;

@Entity()
@Table(name = "customer_type")
public class CustomerType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_type_id")
    private Integer customerTypeId;

    @Column(name = "customer_type_name", length = 100)
    private String customerTypeName;

    @OneToMany(mappedBy = "customerType")
    private List<Customer> customer;

    @OneToMany(mappedBy="customerType")
    private List<ProductPrice> productPrice;


    public Integer getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(Integer customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }
}
