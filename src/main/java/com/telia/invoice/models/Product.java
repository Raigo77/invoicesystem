package com.telia.invoice.models;

import javax.persistence.*;
import java.util.List;

@Entity()
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "product_name", length = 100, nullable = false)
    private String productName;

    @OneToMany(mappedBy="product")
    private List<ProductPrice> productPrice;

    @OneToMany(mappedBy = "product")
    private List<InvoiceRow> invoiceRow;
}
