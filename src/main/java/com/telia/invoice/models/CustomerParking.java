package com.telia.invoice.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "customer_parking")
public class CustomerParking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_parking_id")
    Integer customerParkingId;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    Customer customer;

    @ManyToOne
    @JoinColumn(name = "parking_house_id")
    private ParkingHouse parkingHouse;

    @Column(name = "parking_start_time")
    private LocalDateTime parkingStartTime;

    @Column(name = "parking_end_time")
    private LocalDateTime parkingEndTime;

    public CustomerParking(){

    }

    public CustomerParking(Customer customer, ParkingHouse parkingHouse, LocalDateTime parkingStartTime, LocalDateTime parkingEndTime) {
        this.customer = customer;
        this.parkingHouse = parkingHouse;
        this.parkingStartTime = parkingStartTime;
        this.parkingEndTime = parkingEndTime;
    }

    public Integer getCustomerParkingId() {
        return customerParkingId;
    }

    public void setCustomerParkingId(Integer customerParkingId) {
        this.customerParkingId = customerParkingId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ParkingHouse getParkingHouse() {
        return parkingHouse;
    }

    public void setParkingHouse(ParkingHouse parkingHouse) {
        this.parkingHouse = parkingHouse;
    }

    public LocalDateTime getParkingStartTime() {
        return parkingStartTime;
    }

    public void setParkingStartTime(LocalDateTime parkingStartTime) {
        this.parkingStartTime = parkingStartTime;
    }

    public LocalDateTime getParkingEndTime() {
        return parkingEndTime;
    }

    public void setParkingEndTime(LocalDateTime parkingEndTime) {
        this.parkingEndTime = parkingEndTime;
    }
}
