package com.telia.invoice.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity()
@Table(name = "invoice_row")
public class InvoiceRow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int invoice_row_id;

    @ManyToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private Double quantity;

    public InvoiceRow(Invoice invoice, Product product, Double quantity) {
        this.invoice = invoice;
        this.product = product;
        this.quantity = quantity;
    }

    public int getInvoice_row_id() {
        return invoice_row_id;
    }

    public void setInvoice_row_id(int invoice_row_id) {
        this.invoice_row_id = invoice_row_id;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
