package com.telia.invoice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity()
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Integer customerId;

    @ManyToOne
    @JoinColumn(name = "customer_type_id")
    @JsonIgnore
    private CustomerType customerType;

    @Column(name = "customer_name", length = 100, nullable = false)
    private String customerName;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<CustomerParking> customerParking;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<Invoice> invoice;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }
}
