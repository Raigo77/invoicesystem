package com.telia.invoice.models;

import javax.persistence.*;
import java.util.List;

@Entity()
@Table(name = "parking_house")
public class ParkingHouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parking_house_id")
    private Integer parkingHouseId;

    @Column(name = "parking_house_name", length = 100)
    private String parkingHouseName;

    @OneToMany(mappedBy = "parkingHouse")
    List<CustomerParking> customerParking;

    public Integer getParkingHouseId() {
        return parkingHouseId;
    }

    public void setParkingHouseId(Integer parkingHouseId) {
        this.parkingHouseId = parkingHouseId;
    }

    public String getParkingHouseName() {
        return parkingHouseName;
    }

    public void setParkingHouseName(String parkingHouseName) {
        this.parkingHouseName = parkingHouseName;
    }
}
