package com.telia.invoice.dto;

import com.telia.invoice.models.ParkingHouse;


import java.time.LocalDateTime;

public class ParkingDto {

    private Integer customerId;
    private Integer parkingHouseId;

    private LocalDateTime parkingStartTime;
    private LocalDateTime parkingEndTime;

    public ParkingDto(Integer customerId, Integer parkingHouseId, LocalDateTime parkingStartTime, LocalDateTime parkingEndTime) {
        this.customerId = customerId;
        this.parkingHouseId = parkingHouseId;
        this.parkingStartTime = parkingStartTime;
        this.parkingEndTime = parkingEndTime;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getParkingHouseId() {
        return parkingHouseId;
    }

    public void setParkingHouseId(Integer parkingHouseId) {
        this.parkingHouseId = parkingHouseId;
    }

    public LocalDateTime getParkingStartTime() {
        return parkingStartTime;
    }

    public void setParkingStartTime(LocalDateTime parkingStartTime) {
        this.parkingStartTime = parkingStartTime;
    }

    public LocalDateTime getParkingEndTime() {
        return parkingEndTime;
    }

    public void setParkingEndTime(LocalDateTime parkingEndTime) {
        this.parkingEndTime = parkingEndTime;
    }
}
