package com.telia.invoice.services;

import com.telia.invoice.dto.CustomerDto;
import com.telia.invoice.models.Customer;
import com.telia.invoice.repositories.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<CustomerDto> getCustomerList(){

        Iterable<Customer> customers = customerRepository.findAll();
        List<CustomerDto> customerDtoList = new ArrayList<CustomerDto>();
        for (Customer customer : customers){
            customerDtoList.add(new CustomerDto(customer.getCustomerId(), customer.getCustomerName()));
        }
        return customerDtoList;
    }

    public CustomerDto getCustomerById(Integer id){

        Customer customer = customerRepository.findById(id).get();
        CustomerDto customerDto = new CustomerDto(customer.getCustomerId(), customer.getCustomerName());

        return customerDto;
    }
}
