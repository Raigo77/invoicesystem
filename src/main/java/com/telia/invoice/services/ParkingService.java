package com.telia.invoice.services;

import com.telia.invoice.dto.ParkingDto;
import com.telia.invoice.models.Customer;
import com.telia.invoice.models.CustomerParking;
import com.telia.invoice.models.ParkingHouse;
import com.telia.invoice.repositories.CustomerRepository;
import com.telia.invoice.repositories.ParkingHouseRepository;
import com.telia.invoice.repositories.ParkingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ParkingService {

    private ParkingRepository parkingRepository;
    private ParkingHouseRepository parkingHouseRepository;
    private CustomerRepository customerRepository;

    public ParkingService(ParkingRepository parkingRepository,
                          ParkingHouseRepository parkingHouseRepository,
                          CustomerRepository customerRepository) {
        this.parkingRepository = parkingRepository;
        this.parkingHouseRepository = parkingHouseRepository;
        this.customerRepository = customerRepository;
    }

    public Boolean customerExists(Integer customerId){
        Boolean customerExists = customerRepository.existsById(customerId);
        return customerExists;
    }

    public Boolean parkingHouseExists(Integer parkingHouseId){
        Boolean parkingHouseExists = customerRepository.existsById(parkingHouseId);
        return parkingHouseExists;
    }

    public void save(ParkingDto parkingDto) {
        parkingRepository.save(parkingDtoToEntity(parkingDto));
    }

    private CustomerParking parkingDtoToEntity (ParkingDto parkingDto){

        ParkingHouse parkingHouse = parkingHouseRepository.findById(parkingDto.getParkingHouseId()).get();

        Customer customer = customerRepository.findById(parkingDto.getCustomerId()).get();

        CustomerParking customerParking = new CustomerParking(
                customer,
                parkingHouse,
                parkingDto.getParkingStartTime(),
                parkingDto.getParkingEndTime()
        );
        return customerParking;
    }

    private ParkingDto parkingEntityToDto (CustomerParking customerParking){

        ParkingDto parkingDto = new ParkingDto(
                customerParking.getCustomer().getCustomerId(),
                customerParking.getParkingHouse().getParkingHouseId(),
                customerParking.getParkingStartTime(),
                customerParking.getParkingEndTime()
        );
        return parkingDto;
    }

    public List<ParkingDto> getCustomerParkings(Integer customerId) {

        Customer customer = customerRepository.findById(customerId).get();

        List<CustomerParking> customerParkings = parkingRepository.findByCustomer(customer);

        List<ParkingDto> parkingDtos = new ArrayList<ParkingDto>();

        for (CustomerParking customerParking : customerParkings)
        {
            parkingDtos.add(parkingEntityToDto(customerParking));
        }

        return parkingDtos;
    }

    public List<ParkingDto> getCustomerParkingsInTimeRange(Integer customerId,
                                                            LocalDateTime startDateTime,
                                                            LocalDateTime endDateTime) {

        Customer customer = customerRepository.findById(customerId).get();

        List<CustomerParking> customerParkings = parkingRepository.
                findByCustomerAndParkingStartTimeGreaterThanEqualAndParkingEndTimeLessThanEqual(
                        customer,
                        startDateTime,
                        endDateTime);

        List<ParkingDto> parkingDtos = new ArrayList<ParkingDto>();

        for (CustomerParking customerParking : customerParkings)
        {
            parkingDtos.add(parkingEntityToDto(customerParking));
        }

        return parkingDtos;
    }

}