package com.telia.invoice.services;

import com.telia.invoice.dto.GenerateInvoiceDto;
import com.telia.invoice.models.*;
import com.telia.invoice.repositories.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;

@Service
public class InvoiceService {

    private ParkingRepository parkingRepository;
    private InvoiceRepository invoiceRepository;
    private CustomerRepository customerRepository;
    private ProductRepository productRepository;
    private ProductPriceRepository productPriceRepository;

    private int periodsBetween7AmAnd7Pm;
    private int periodsBetween7PmAnd7Am;
    double maxInvoiceSum;

    public InvoiceService(ParkingRepository parkingRepository,
                          InvoiceRepository invoiceRepository,
                          CustomerRepository customerRepository,
                          ProductRepository productRepository,
                          ProductPriceRepository productPriceRepository
                          ) {
        this.parkingRepository = parkingRepository;
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.productPriceRepository = productPriceRepository;
        this.periodsBetween7AmAnd7Pm = 2; //TODO: values from conf
        this.periodsBetween7PmAnd7Am = 3;
        this.maxInvoiceSum = 300;

        /*
         * 2 - Parking fee for every started half hour (from 7am till 7pm)
         * 3 - Parking fee for every started half hour (from 7pm till 7am)
         * */
    }

    public void generateInvoice(GenerateInvoiceDto generateInvoiceDto){

        Customer customer = customerRepository.findById(generateInvoiceDto.getCustomerId()).get();

        Double productPrice7AmTo7pm = getProductPriceByCustomerTypeAndProductId(
                customer.getCustomerType(),
                periodsBetween7AmAnd7Pm).getPrice();

        Double productPrice7PmTo7Am = getProductPriceByCustomerTypeAndProductId(
                customer.getCustomerType(),
                periodsBetween7PmAnd7Am).getPrice();

        int monthlyFeeProductId = 3;
        Double productPriceMonthlyFee = getProductPriceByCustomerTypeAndProductId(
                customer.getCustomerType(),
                monthlyFeeProductId).getPrice();

        List<CustomerParking> customerParkingsInRange =
                parkingRepository.findByCustomerAndParkingStartTimeGreaterThanEqualAndParkingEndTimeLessThanEqual(
                        customer,
                        generateInvoiceDto.getStartTime(),
                        generateInvoiceDto.getEndTime());


        HashMap<Integer,Integer> invoicePeriodsTotal = new HashMap<Integer,Integer>();

        invoicePeriodsTotal.put(periodsBetween7AmAnd7Pm, 0);
        invoicePeriodsTotal.put(periodsBetween7PmAnd7Am, 0);

        for (CustomerParking customerParking : customerParkingsInRange)
        {
            HashMap<Integer,Integer> parkingPeriods = getPeriodsCount(customerParking.getParkingStartTime(),
                     customerParking.getParkingEndTime());

            invoicePeriodsTotal.put(periodsBetween7AmAnd7Pm, invoicePeriodsTotal.get(periodsBetween7AmAnd7Pm) +
                    parkingPeriods.get(periodsBetween7AmAnd7Pm));

            invoicePeriodsTotal.put(periodsBetween7PmAnd7Am, invoicePeriodsTotal.get(periodsBetween7PmAnd7Am) +
                    parkingPeriods.get(periodsBetween7PmAnd7Am));
        }

        Double invoiceSum = (invoicePeriodsTotal.get(periodsBetween7AmAnd7Pm) * productPrice7AmTo7pm) +
                (invoicePeriodsTotal.get(periodsBetween7PmAnd7Am) * productPrice7PmTo7Am) +
                productPriceMonthlyFee;

        if (invoiceSum > maxInvoiceSum){
            invoiceSum = maxInvoiceSum;
        }

        invoiceRepository.save(new Invoice(customer, invoiceSum));

        //TODO: insert invoiceRows
    }

    private ProductPrice getProductPriceByCustomerTypeAndProductId(CustomerType customerType, int productId){

        Product product = productRepository.findById(productId).get();
        ProductPrice productPrice = productPriceRepository.findByCustomerTypeAndProduct(
                customerType,
                product);

        return productPrice;
    }

    private HashMap<Integer,Integer> getPeriodsCount(LocalDateTime parkingStartTime, LocalDateTime parkingEndTime){

        int duration = 30;

        HashMap<Integer,Integer> periodsCount = new HashMap<Integer,Integer>();

        periodsCount.put(periodsBetween7AmAnd7Pm, 0);
        periodsCount.put(periodsBetween7PmAnd7Am, 0);

        while (parkingStartTime.isBefore(parkingEndTime))
        {
            LocalTime sevenAm = LocalTime.of(7, 00);
            LocalTime sevenPm = LocalTime.of(19, 00);

            LocalTime periodStartTime = parkingStartTime.toLocalTime();
            if (periodStartTime.isAfter(sevenAm) && periodStartTime.isBefore(sevenPm)){
                periodsCount.put(periodsBetween7AmAnd7Pm, periodsCount.getOrDefault(periodsBetween7AmAnd7Pm, 0) + 1);
            } else {
                periodsCount.put(periodsBetween7PmAnd7Am, periodsCount.getOrDefault(periodsBetween7PmAnd7Am, 0) + 1);
            }
            parkingStartTime = parkingStartTime.plusMinutes(duration);
        }

        return periodsCount;
    }
}
