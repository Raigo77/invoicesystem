package com.telia.invoice.controllers;

import com.telia.invoice.dto.GenerateInvoiceDto;
import com.telia.invoice.services.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/invoices")
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;


    @PostMapping
    @ResponseBody
    public ResponseEntity createInvoice(@RequestBody final GenerateInvoiceDto generateInvoiceDto){

        invoiceService.generateInvoice(generateInvoiceDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
