package com.telia.invoice.controllers;

import com.telia.invoice.dto.Pair;
import com.telia.invoice.dto.ParkingDto;
import com.telia.invoice.services.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/parkings")
public class ParkingController {
    @Autowired
    private ParkingService parkingService;


    @GetMapping
    @RequestMapping("/customer/{id}")
    public List<ParkingDto> getCustomerParkings(@PathVariable Integer id){

        return parkingService.getCustomerParkings(id);
    }

    @GetMapping
    @RequestMapping("/customer/{id}/start/{startDateTime}/end/{endDateTime}")
    public List<ParkingDto> getCustomerParkingsBetweenDates(@PathVariable Integer id,
                                                            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDateTime,
                                                            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDateTime){

        return parkingService.getCustomerParkingsInTimeRange(id, startDateTime, endDateTime);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity create(@RequestBody final ParkingDto parkingDto){

        Pair<Boolean, ArrayList> isValid = isParkingDtoValid(parkingDto);
        if (!isValid.getKey()) {
            return new ResponseEntity<>(
                    isValid.getValue(),
                    HttpStatus.BAD_REQUEST);
        }

        parkingService.save(parkingDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }



    private Pair<Boolean, ArrayList> isParkingDtoValid(ParkingDto parkingDto){
        Boolean isValid = true;
        ArrayList messages = new ArrayList();

        if (!parkingService.customerExists(parkingDto.getCustomerId())){
            isValid = false;
            messages.add("Customer with given id  does not exist.");
        }

        if (!parkingService.parkingHouseExists(parkingDto.getParkingHouseId())){
            isValid = false;
            messages.add("Parking house with given id does not exist.");
        }

        if (parkingDto.getParkingEndTime().isBefore(parkingDto.getParkingStartTime())) {
            isValid = false;
            messages.add("Start date must be smaller than end date.");
        }

        return new Pair<>(isValid, messages);
    }

}
