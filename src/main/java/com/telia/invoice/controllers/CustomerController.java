package com.telia.invoice.controllers;

import com.telia.invoice.dto.CustomerDto;
import com.telia.invoice.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<CustomerDto> customers(){
        return customerService.getCustomerList();
    }


    @GetMapping
    @RequestMapping("{id}")
    public CustomerDto customerById(@PathVariable Integer id){
        return customerService.getCustomerById(id);
    }

}
