package com.telia.invoice.repositories;

import com.telia.invoice.models.CustomerType;
import com.telia.invoice.models.Product;
import com.telia.invoice.models.ProductPrice;
import org.springframework.data.repository.CrudRepository;

public interface ProductPriceRepository extends CrudRepository<ProductPrice, Integer> {

    public ProductPrice findByCustomerTypeAndProduct(CustomerType customerType, Product product);
}
