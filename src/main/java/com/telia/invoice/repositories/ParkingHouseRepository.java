package com.telia.invoice.repositories;

import com.telia.invoice.models.ParkingHouse;
import org.springframework.data.repository.CrudRepository;

public interface ParkingHouseRepository extends CrudRepository<ParkingHouse, Integer> {
}
