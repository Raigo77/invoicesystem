package com.telia.invoice.repositories;

import com.telia.invoice.models.InvoiceRow;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRowRepository extends CrudRepository<InvoiceRow, Integer> {
}
