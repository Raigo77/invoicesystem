package com.telia.invoice.repositories;

import com.telia.invoice.models.Customer;
import com.telia.invoice.models.CustomerParking;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


public interface ParkingRepository extends CrudRepository<CustomerParking, Integer> {

    public List<CustomerParking> findByCustomer(Customer customer);

    public List<CustomerParking> findByCustomerAndParkingStartTimeGreaterThanEqualAndParkingEndTimeLessThanEqual(Customer customer,
                                                                              LocalDateTime startTime,
                                                                              LocalDateTime endTime);

}
