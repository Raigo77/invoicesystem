INSERT INTO product (product_id, product_name) VALUES (1, 'Monthly fee');
INSERT INTO product (product_id, product_name) VALUES (2, 'Parking fee for every started half hour (from 7am till 7pm)');
INSERT INTO product (product_id, product_name) VALUES (3, 'Parking fee for every started half hour (from 7pm till 7am)');


INSERT INTO customer_type (customer_type_id, customer_type_name) VALUES (1, 'Regular');
INSERT INTO customer_type (customer_type_id, customer_type_name) VALUES (2, 'Premium');

INSERT INTO product_price (product_id, customer_type_id, price) VALUES (1, 1, 0);
INSERT INTO product_price (product_id, customer_type_id, price) VALUES (1, 2, 20);

INSERT INTO product_price (product_id, customer_type_id, price) VALUES (2, 1, 1.50);
INSERT INTO product_price (product_id, customer_type_id, price) VALUES (2, 2, 1);

INSERT INTO product_price (product_id, customer_type_id, price) VALUES (3, 1, 1);
INSERT INTO product_price (product_id, customer_type_id, price) VALUES (3, 2, 0.75);

INSERT INTO customer(customer_type_id, customer_name) values (1, 'Test Regular customer');
INSERT INTO customer(customer_type_id, customer_name) values (2, 'Test Premium customer');

INSERT INTO parking_house (parking_house_name) VALUES ('Test parking house');
